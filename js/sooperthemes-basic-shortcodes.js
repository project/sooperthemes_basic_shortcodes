(function($) {
    var p = '';
    var fp = '';
    if ('azexo_prefix' in window) {
        p = window.azexo_prefix;
        fp = window.azexo_prefix.replace('-', '_');
    }

    function t(text) {
        if ('azexo_t' in window) {
            return window.azexo_t(text);
        } else {
            return text;
        }
    }
    var colors = window.sooperthemes_colors;
    colors[''] = t('Custom');
    colors['auto'] = t('Auto');
    colors['inherit'] = t('Inherit');

    var azexo_elements = [
        {
            base: 'st_social',
            name: t('Social links'),
            category: t('Sooper Themes'),
            icon: 'fa fa-twitter',
            params: [
                {
                    type: 'html',
                    heading: t('Social links'),
                    param_name: 'st_social_links',
                },
                {
                    type: 'dropdown',
                    heading: t('Layout'),
                    param_name: 'st_type',
                    value: {
                        '': t('None'),
                        'inline': t('Inline'),
                        'stacked': t('Stacked'),
                    },
                },
                {
                    type: 'dropdown',
                    heading: t('Style'),
                    param_name: 'st_style',
                    value: {
                        '': t('None'),
                        'rounded': t('Rounded'),
                        'circle': t('Circle'),
                        'square': t('Square'),
                    },
                },
                {
                    type: 'dropdown',
                    heading: t('Size'),
                    param_name: 'st_size',
                    value: {
                        '': t('Small'),
                        'lg': t('Large'),
                        '2x': t('2x'),
                        '3x': t('3x'),
                        '4x': t('4x'),
                        '5x': t('5x'),
                    },
                },
                {
                    type: 'dropdown',
                    heading: t('Color from theme'),
                    param_name: 'st_theme_color',
                    value: colors,
                },
                {
                    type: 'colorpicker',
                    heading: t('Color'),
                    param_name: 'st_color',
                    dependency: {'element': 'st_theme_color', 'is_empty': {}},
                },
                {
                    type: 'dropdown',
                    heading: t('Hover color'),
                    param_name: 'st_hover_color',
                    value: {
                        '': t('None'),
                        'auto': t('Brand color'),
                        'inherit': t('Inherit from theme'),
                    },
                },
                {
                    type: 'dropdown',
                    heading: t('Border color from theme'),
                    param_name: 'st_theme_border_color',
                    value: colors,
                },
                {
                    type: 'colorpicker',
                    heading: t('Border color'),
                    param_name: 'st_border_color',
                    dependency: {'element': 'st_theme_border_color', 'is_empty': {}},
                },
                {
                    type: 'dropdown',
                    heading: t('CSS3 Hover Effects'),
                    param_name: 'st_css3_hover_effects',
                    value: {
                        '': t('None'),
                        'disc': t('Disc'),
                        'pulse': t('Pulse'),
                    },
                },
            ],
            show_settings_on_create: true,
            render: function($, p, fp) {
                this.dom_element = $('<ul class="az-element st-social social-links ' + this.attrs['el_class'] + '" style="' + this.attrs['style'] + '"></ul>');
                if (this.attrs['st_theme_color'] == 'auto')
                    $(this.dom_element).addClass('icons-color-auto');
                if (this.attrs['st_hover_color'] == 'auto')
                    $(this.dom_element).addClass('icons-color-hover-auto');
                if (this.attrs['st_type'] != '')
                    $(this.dom_element).addClass(this.attrs['st_type']);

                var icon_style = '';
                if (this.attrs['st_theme_color'] == '') {
                    icon_style = 'background-color: ' + this.attrs['st_color'] + ';';
                } else {
                    if('sooperthemes_theme_palette' in window && window.sooperthemes_theme_palette != null && this.attrs['st_theme_color'] in window.sooperthemes_theme_palette)
                        icon_style = 'background-color: ' + window.sooperthemes_theme_palette[this.attrs['st_theme_color']] + ';';
                    else
                        icon_style = 'background-color: ' + this.attrs['st_theme_color'] + ';';
                }
                if (this.attrs['st_theme_border_color'] == '') {
                    icon_style = 'border-color: ' + this.attrs['st_border_color'] + ';';
                } else {
                    if('sooperthemes_theme_palette' in window && window.sooperthemes_theme_palette != null && this.attrs['st_theme_border_color'] in window.sooperthemes_theme_palette)
                        icon_style = 'border-color: ' + window.sooperthemes_theme_palette[this.attrs['st_theme_border_color']] + ';';
                    else
                        icon_style = 'border-color: ' + this.attrs['st_theme_border_color'] + ';';
                }


                var links = this.attrs['st_social_links'].split("\n");
                for (var i in links) {
                    if (links[i] != '') {
                        var link = links[i].split("=");
                        var name = link[0];
                        var url = link[1];


                        var icon_classes = ['fa'];
                        icon_classes.push('fa-' + this.attrs['st_size']);
                        icon_classes.push('fa-' + name);
                        icon_classes.push('icon-' + this.attrs['st_css3_hover_effects']);
                        icon_classes.push('icon-' + this.attrs['st_style']);
                        if (this.attrs['st_css3_hover_effects'] == 'pulse')
                            icon_classes.push('icon-fx icon-fx-pulse');
                        if (this.attrs['st_css3_hover_effects'] == 'disc')
                            icon_classes.push('icon-fx icon-fx-disc');
                        if (this.attrs['st_border_color'] != '' || this.attrs['st_theme_border_color'] != '')
                            icon_classes.push('icon-border');


                        $(this.dom_element).append('<li><a href="' + url + '"><i class="' + icon_classes.join(' ') + '" style="' + icon_style + '" data-toggle="tooltip" data-placement="top" title="' + name + '"></i></a></li>')
                    }
                }
                this.baseclass.prototype.render.apply(this, arguments);
            },
        },
    ];

    if ('azexo_elements' in window) {
        window.azexo_elements = window.azexo_elements.concat(azexo_elements);
    } else {
        window.azexo_elements = azexo_elements;
    }
})(window.jQuery);